ifdef SystemRoot
	RM = del /S
else
	ifeq ($(shell uname), Linux)
		RM = rm -r
	endif
endif

bin/account.exe : main.o entry.o entry_prompt.o money.o ledger_reader.o cmdargs.o
	g++ $(objects) $(warnings) -std=c++11 -O3 -o bin/account.exe

cmdargs.o : src/cmdargs.cpp
	g++ $(compile) $(warnings) src/cmdargs.cpp

money.o : src/money.cpp
	g++ $(compile) $(warnings) src/money.cpp
	
entry.o : src/entry.cpp inc/money.h
	g++ $(compile) $(warnings) src/entry.cpp
	
entry_prompt.o : src/entry_prompt.cpp inc/entry.h
	g++ $(compile) $(warnings) src/entry_prompt.cpp
	
ledger_reader.o : src/ledger_reader.cpp inc/entry.h inc/error.h inc/money.h
	g++ $(compile) $(warnings) src/ledger_reader.cpp

	
main.o : src/main.cpp inc/entry.h inc/entry_prompt.h inc/ledger_reader.h inc/error.h inc/money.h
	g++ $(compile) $(warnings) src/main.cpp
	
clean :
	$(RM) *.o *.exe *.csv *.acc

test.exe : cmdargs.o
	g++ test.cpp cmdargs.o -std=c++11 -I inc -o test.exe

	
warnings = -Wfatal-errors -Wall -Wextra -pedantic
compile = -I inc -c -O3 -std=c++11
objects = main.o entry.o entry_prompt.o ledger_reader.o money.o cmdargs.o
