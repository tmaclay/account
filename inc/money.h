#ifndef _TM_MONEY_H
#define _TM_MONEY_H

#include <string>
#include <sstream>
#include <algorithm>

class Money {

	private:
		int dollars;
		int cents;
		bool notSane;
		
	public:
		Money(std::string);
		Money(int, int);
		Money();
		Money(const Money&) = default;
		Money(Money&& other) = default;
		Money& operator=(Money&);
		Money& operator+=(Money&);
		Money& operator*=(int);
		Money& operator*=(double);
		Money& operator-=(Money&);
		Money& operator=(Money&&);


		void parseString(std::string);
		std::string toString();
		bool checkSanity();
		
		int getDollars() const;
		int getCents() const;
		
		void setDollars(int);
		void setCents(int);
		
		friend void swap(Money& first, Money& second) {
			std::swap(first.dollars, second.dollars);
			std::swap(first.cents, second.cents);
		}

};

inline Money operator+(Money lhs, Money& rhs) {
	lhs += rhs;
	return lhs;
}

inline Money operator*(Money lhs, int rhs) {
	lhs *= rhs;
	return lhs;
}

inline Money operator*(Money lhs, double rhs) {
	lhs *= rhs;
	return lhs;
}

inline Money operator-(Money lhs, Money& rhs) {
	lhs -= rhs;
	return lhs;
}

inline bool operator==(Money lhs, Money rhs) {
	return (lhs.getDollars() == rhs.getDollars() && lhs.getCents() == rhs.getCents());
}

inline bool operator<(Money lhs, Money rhs) {
	if(lhs.getDollars() < rhs.getDollars())
		return true;
	else if(lhs.getDollars() == rhs.getDollars() && lhs.getCents() < rhs.getCents())
		return true;
	else
		return false;
}

inline bool operator>=(Money lhs, Money rhs) {
	return !(lhs < rhs);
}

inline bool operator>(Money lhs, Money rhs){
	return rhs < lhs;
}

inline bool operator<=(Money lhs, Money rhs){
	return !(rhs < lhs);
}

inline bool operator!=(Money lhs, Money rhs){
	return !(lhs == rhs);
}

#endif