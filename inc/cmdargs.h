#ifndef _ACCOUNT_CMDARGS_H
#define _ACCOUNT_CMDARGS_H

#include <sstream>
#include <string>
#include <vector>

#include "error.h"

class cmdargs {
	private:
		std::vector<std::string> args;

	public:
		cmdargs(int argc, char* argv[]);
		int findOpt(std::string);
		std::string getOpt(int);

};

#endif