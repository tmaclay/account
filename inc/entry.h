#ifndef _ACCOUNT_ENTRY_01
#define _ACCOUNT_ENTRY_01

//entry.h
//Last updated 5/2/2013

//LAYOUT
//
// Need date container --> Int yymmdd
// Need ability to write reason --> String
// Need ability to tell amount --> Float
// Need standard way to tell if debit or credit --> Bool?
//
//END LAYOUT

//Includes

#include <string>
#include <sstream>
#include "money.h"

//End Includes

class Entry {

	private:
		unsigned int date;
		std::string reason;
		Money amount;
		bool debit;
		unsigned int entryNum;
		
	public:
		Entry(unsigned int, std::string, std::string, bool);
		Entry(unsigned int, std::string, std::string, bool, unsigned int);
		Entry() {};
		std::string writeOutConsole();
		std::string writeOutCSVFile();
		std::string writeOutProgramFile();
		
		
		unsigned int getDate() const;
		std::string getReason() const;
		std::string getAmountStr();
		Money getAmount() const;
		float getAmountVal() const;
		bool getDebit() const;
		unsigned int getEntryNum() const;

		void setEntryNum(unsigned int);

		static bool compareByDate(const Entry& lhs, const Entry& rhs) { return (lhs.getDate() < rhs.getDate()); }
		static bool compareByAmount(const Entry& lhs, const Entry& rhs) { return (lhs.getAmountVal() < rhs.getAmountVal()); }
		static bool compareByEntryNum(const Entry& lhs, const Entry& rhs) { return (lhs.getEntryNum() < rhs.getEntryNum()); }
		static bool compareByDebitStatus(const Entry& lhs, const Entry& rhs) { return ((int)lhs.getDebit() < (int)rhs.getDebit()); }
};

// Work that needs doing:
// Need to handle when the string "reason" contains commas, makes csv files difficult. Will work on it.

#endif