#ifndef _ACCOUNT_ERROR_H
#define _ACCOUNT_ERROR_H

#include <stdexcept>
#include <string>

class AccountError : public std::runtime_error
{
	public:
		AccountError(const std::string& emsg)
			: std::runtime_error(emsg)
			{
			
			}
};

#endif