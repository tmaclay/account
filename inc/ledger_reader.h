#ifndef _ACCOUNT_LEDGER_READER
#define _ACCOUNT_LEDGER_READER 01

//ledger_reader.h
//Last updated 5/2/2013


//Need to be able to accept csv file in the form of date/reason/amount/debit

//FOR FUTURE!!
//Update to allow one to search for particular instance of an entry.
//Actually, scratch that noise above. Searching for particular entry is rather pointless
//...as you would already need to know the information contained within the entry to find it.
//To implement could implement some sort of numbering system independant of the four values already stated.

//Includes

#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <functional>

#include "entry.h"
#include "error.h"

//End Includes

class Account_Ledger {
	
	private:
		std::ifstream in;
		std::vector<Entry> ledger;
		unsigned int number_of_entries;
		std::string fileName;
		
	public:
		Account_Ledger(std::string); //String should be a filename
		bool getNext(); //Returns false when extract fails
		Entry getEntry(unsigned int); //Get single entry. Need to develop a way to retrieve multiple entries
		std::vector<Entry> getRange(unsigned int, unsigned int);
		//Use the entry number field! Also do checks when adding entries to ledger!
		unsigned int getNumberOfEntries();
		void addToLedger(Entry);
		void updateLedgerFile();
		void deleteFromLedger(unsigned int);
		void sortLedger(int);
		void switchLedger(std::string);
		void addLedger(std::string);

};

#endif