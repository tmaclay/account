//entry_prompt.h

#ifndef _ENTRY_PROMPT_ACCOUNT
#define _ENTRY_PROMPT_ACCOUNT

//Includes

#include "entry.h"
#include "error.h"
#include <iostream>

//End Includes

class Entry_Writer {
	private:
		unsigned int numWritten;
		bool getDebitStatus(char charDebit);
		
	public:
		Entry_Writer();
		Entry makeEntry();
		
};

#endif

//INTEGRATE ERROR CATCHING!!!!!
