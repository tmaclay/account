//ledger_reader.cpp

//Includes

#include "ledger_reader.h"

//Test
#include <iostream>
//End Test

Account_Ledger::Account_Ledger(std::string filname) {
	number_of_entries = 0;
	in.open(filname.c_str());
	fileName = filname;
	while(getNext());
	in.close();
}

bool Account_Ledger::getNext() {
	std::string tempLine;
	std::stringstream ss;
	std::stringstream ssTemp;
	std::string tempValue;
	
	unsigned int date;
	std::string reason;
	std::string amount;
	bool debit;
	
	if(std::getline(in,tempLine)) {
	
		ss << tempLine;
		std::getline(ss,tempValue,(char)0x6);
		ssTemp << tempValue.substr(1,tempValue.size()-2);
		ssTemp >> date;
		tempValue.clear();
		ssTemp.str(std::string());
		ssTemp.clear();
		
		std::getline(ss,tempValue,(char)0x6);
		ssTemp << tempValue.substr(1,tempValue.size()-2);
		reason = ssTemp.str();
		tempValue.clear();
		ssTemp.str(std::string());
		ssTemp.clear();
		
		std::getline(ss,tempValue,(char)0x6);
		ssTemp << tempValue.substr(1,tempValue.size()-2);
		amount = ssTemp.str();
		tempValue.clear();
		ssTemp.str(std::string());
		ssTemp.clear();
		
		std::getline(ss,tempValue,(char)0x6);
		ssTemp << tempValue.substr(1,tempValue.size()-2);
		ssTemp >> debit;
		tempValue.clear();
		ssTemp.str(std::string());
		ssTemp.clear();
		
		ledger.push_back(Entry(date,reason,amount,debit,number_of_entries));
		number_of_entries++;
		return true;
	}
	
	return false;
		
	
}

Entry Account_Ledger::getEntry(unsigned int spot) {
	
	for(std::vector<Entry>::iterator it = ledger.begin(); it != ledger.end(); ++it) {
		if(it->getEntryNum() == spot)
			return Entry(it->getDate(),it->getReason(),it->getAmount().toString(),it->getDebit(),it->getEntryNum()); //Returns new Entry object rather than reference
	}
	

	throw AccountError("Entry Not Found!");
}

void Account_Ledger::deleteFromLedger(unsigned int spot) {
	bool erased = false;
	
	for(std::vector<Entry>::iterator it = ledger.begin(); it != ledger.end(); ++it) {
		if(it->getEntryNum() == spot) {
			ledger.erase(it);
			erased = true;
		}
	}
	
	if(!erased)
		throw AccountError("Entry does not exist!");
}

unsigned int Account_Ledger::getNumberOfEntries() {
	return number_of_entries;
}

void Account_Ledger::addToLedger(Entry newEntry) {
	newEntry.setEntryNum(number_of_entries);
	number_of_entries++;
	ledger.push_back(newEntry);
}

void Account_Ledger::updateLedgerFile() {
	std::ofstream out(fileName.c_str(), std::ios::trunc | std::ios::out);
	std::string outString;
	std::vector<Entry>::iterator it = ledger.begin();
	outString = it->writeOutProgramFile(); //Prime the file
	out << outString;
	++it;
	for(; it != ledger.end(); ++it) { //Step through the vector, adding each to the end of the file
		out << std::endl;
		outString = it->writeOutProgramFile();
		out << outString;
	}
	
	out.close();
}

std::vector<Entry> Account_Ledger::getRange(unsigned int low, unsigned int high) {
	std::vector<Entry> retVector;
	for(std::vector<Entry>::iterator it = ledger.begin(); it != ledger.end(); ++it) {
		if( it->getEntryNum() >= low && it->getEntryNum() <= high )
			retVector.push_back(Entry(it->getDate(),it->getReason(),it->getAmount().toString(),it->getDebit(),it->getEntryNum()));
	}
	
	
	return retVector;
}

void Account_Ledger::sortLedger(int sortBy) {
	switch(sortBy) {
		case 0:
			std::sort(ledger.begin(), ledger.end(), Entry::compareByDate);
			updateLedgerFile();
			break;
		case 1:
			std::sort(ledger.begin(), ledger.end(), Entry::compareByAmount);
			updateLedgerFile();
			break;
		case 2:
			std::sort(ledger.begin(), ledger.end(), Entry::compareByEntryNum);
			updateLedgerFile();
			break;
		case 3:
			std::sort(ledger.begin(), ledger.end(), Entry::compareByDebitStatus);
			updateLedgerFile();
			break;
			
		default:
			throw AccountError("Invalid Sorting Preferance!");
		}
}

void Account_Ledger::switchLedger(std::string newFileName) {
	in.open(newFileName.c_str());

	ledger.clear();
	number_of_entries = 0;
	
	fileName = newFileName;
	
	while(getNext());
	
	in.close();
}

void Account_Ledger::addLedger(std::string secondLedger) {
	in.open(secondLedger.c_str());
	if(!(in.is_open()))
		throw AccountError("Unable to open new ledger!");
		
	while(getNext());
		
	in.close();
}
	
