//Account Main

//Definitions
#define VERSION "0.85.1"

//End Definitions

//Includes

#include <iostream>
#include <fstream>
#include <limits>

#include "entry.h"
#include "entry_prompt.h"
#include "error.h"
#include "ledger_reader.h"
#include "cmdargs.h"

//End Includes

int main(int argc, char** argv) {

	try {

		cmdargs cmd(argc, argv);
		std::cout<<"Personal Ledger v"<<VERSION<<std::endl;
		std::string testString = "test.acc";
		Account_Ledger csv(testString);
		Entry_Writer ent;
		Entry enter;
		char choice1;
		char choice2;
		bool keepRunning = true;
		bool changed = false;
		bool subRun;
		unsigned int spot;
		int sortNum;
		std::string newFile;
		
		while(keepRunning) {
			subRun = true;
			//std::cout<<"(N)ew entry, (D)elete entry, (U)pdate Ledger, (S)ort Ledger, (C)heck Value, (Q)uit: ";
			std::cout<<"(E)ntry Options, (L)edger Options, (Q)uit: ";
			std::cin >> choice1;
			switch(choice1) {
				case 'e':
				case 'E':
					while(subRun) {
						std::cout<<"(N)ew entry, (D)elete entry, (C)heck Value, (B)ack: ";
						std::cin>>choice2;
					
						switch(choice2) {
							case 'N':
							case 'n':
								csv.addToLedger(ent.makeEntry());
								changed = true;
								break;
					
							case 'D':
							case 'd':
								std::cout<<"Enter entry to be deleted: ";
								std::cin>>spot;
								csv.deleteFromLedger(spot);
								changed = true;
								break;
							
							case 'B':
							case 'b':
								subRun = false;
								break;
								
							case 'C':
							case 'c':
								std::cout<<"Enter Entry Number: ";
								std::cin>>spot;
								std::cout<<csv.getEntry(spot).writeOutConsole();
								break;
								
							
							default:
								std::cout<<"You must enter a letter in parenthesis!" << std::endl;
							}
					}
					
					break;
				
				
				case 'L':
				case 'l':
					while(subRun) {
						std::cout<<"(A)dd Ledger, (S)witch Ledger, (U)pdate Ledger, So(r)t Ledger, (B)ack: ";
						std::cin>>choice2;
						
						switch(choice2){
				
							case 'U':
							case 'u':
								if(changed)
									csv.updateLedgerFile();
								break;
								
							case 'R':
							case 'r':
								std::cout<<"Enter Sort Method..." << std::endl;
                                std::cout<<"0 to sort by date" <<std::endl;
                                std::cout<<"1 to sort by amount"<<std::endl;
                                std::cout<<"2 to sort by entry number"<<std::endl;
                                std::cout<<"3 to sort by debit status"<<std::endl;
                                std::cout<<"Choose number: ";
                                std::cin>>sortNum;
								
                                csv.sortLedger(sortNum);
                                break;
								
							case 'A':
							case 'a':
                                std::cout<<"Enter name of account: ";
								std::cin.ignore(std::numeric_limits<long int>::max(), '\n');
								std::getline(std::cin,newFile);
								csv.addLedger(newFile);
								break;
							
							case 'S':
							case 's':
								std::cout<<"Enter name of account: ";
								std::cin.ignore(std::numeric_limits<long int>::max(), '\n');
								std::getline(std::cin, newFile);
								csv.switchLedger(newFile);
								break;
							
							case 'b':
							case 'B':
								subRun = false;
								break;
							
							default:
								std::cout<<"You must enter a letter in parenthesis!" << std::endl;
							}
					}
						
					break;
					
				case 'q':
				case 'Q':
					keepRunning = false;
					break;
				
				default:
					std::cout<<"You must enter a letter in parenthesis!" << std::endl;
				
				}
			}
			if(changed) {
				std::cout<<"Auto-Updating Ledger File...";
				csv.updateLedgerFile();
				std::cout<<"Done..." << std::endl;
			}
			
			std::cout<<"Exiting..." << std::endl;
				
			

	} catch(AccountError& e) {
		std::cout<<e.what();
		std::cout<<std::endl;
	}
	
	
	return 0;
}