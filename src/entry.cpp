//Entry.cpp

//Includes

#include "entry.h"

//End Includes

Entry::Entry(unsigned int date_in, std::string reason_in, std::string amount_in, bool debit_in) {
	date = date_in;
	reason = reason_in;
	amount.parseString(amount_in);
	debit = debit_in;
}

Entry::Entry(unsigned int date_in, std::string reason_in, std::string amount_in, bool debit_in, unsigned int numberE) {
	date = date_in;
	reason = reason_in;
	amount.parseString(amount_in);
	debit = debit_in;
	entryNum = numberE;
}

std::string Entry::writeOutConsole() {
	std::stringstream retVal;
	retVal << date;
	retVal << " ";
	retVal << "\"" << reason << "\"";
	retVal << " ";
	retVal << amount.toString();
	retVal << " ";
	retVal << debit;
	retVal << std::endl;
	
	return retVal.str();
}

std::string Entry::writeOutCSVFile() {
	std::stringstream retVal;
	retVal <<"\"" << date << "\"";
	retVal << ",";
	retVal << "\"" << reason << "\"";
	retVal << ",";
	retVal << "\"" << amount.toString() << "\"";
	retVal << ",";
	retVal << "\"" << debit << "\"";
	
	return retVal.str();
}

std::string Entry::writeOutProgramFile() {
	std::stringstream retVal;
	retVal << "\"" << date << "\"";
	retVal << (char)0x6;
	retVal << "\"" << reason << "\"";
	retVal << (char)0x6;
	retVal << "\"" << amount.toString() << "\"";
	retVal << (char)0x6;
	retVal << "\"" << debit << "\"";
	
	return retVal.str();
}


unsigned int Entry::getDate() const {
	return date;
}

std::string Entry::getReason() const {
	return reason;
}

Money Entry::getAmount() const {
	return amount;
}

std::string Entry::getAmountStr() {
	return amount.toString();
}

bool Entry::getDebit() const {
	return debit;
}

void Entry::setEntryNum(unsigned int num) {
	entryNum = num;
}

unsigned int Entry::getEntryNum() const {
	return entryNum;
}

float Entry::getAmountVal() const {
	return ((float)amount.getDollars() + (float)amount.getCents()/100.);
}