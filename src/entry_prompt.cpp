//entry_prompt.cpp


//Includes

#include "entry_prompt.h"
#include <limits>

//End Includes


template <typename T>
T getInput(const std::string& prompt) {
	T n;
	while( (std::cout<< prompt) && !(std::cin>>n)) {
		std::cout<<"Invalid Input!" << std::endl;
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<int>::max(), '\n');
	}
	
	return n;
}

Entry_Writer::Entry_Writer() {
	numWritten = 0;
}

Entry Entry_Writer::makeEntry() {

	std::string reason;
	std::string amt;
	unsigned int date;
	char debit;
	
	date = getInput<unsigned int>("Enter date (yymmdd): ");
	
	std::cin.clear();
	std::cin.sync();
	
	std::cout<<"Enter Usage: ";
	std::getline(std::cin,reason);
	
	std::cout<<"Enter amount: ";
	std::getline(std::cin,amt);
	
	std::cin.clear();
	std::cin.sync();
	
	debit = getInput<char>("Is this debit (y/n): ");
	
	std::cin.clear();
	std::cin.sync();
	
	Entry retVal(date,reason,amt,getDebitStatus(debit));

	
	return retVal;
}

bool Entry_Writer::getDebitStatus(char charDebit) {
	
	switch(charDebit) {
		
		case 'Y':
		case 'y':
			return true;
			
		case 'N':
		case 'n':
			return false;
			
		default:
			throw AccountError("Invalid Input for debit/credit!");
	}
}
