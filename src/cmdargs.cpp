#include "cmdargs.h"

cmdargs::cmdargs(int argc, char* argv[]) {
	for(int i=0; i < argc; ++i) {
		args.push_back(argv[i]);
	}
}

int cmdargs::findOpt(std::string opt) {
	for(unsigned int i=0; i < args.size(); ++i){
		if(!opt.compare(args[i]))
			return i;
	}
	throw AccountError("Error! Argument Not Found!");
}

std::string cmdargs::getOpt(int optToGet){
	if(optToGet != -1)
		return args[optToGet];
	else return std::string("");
}
