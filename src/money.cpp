#include "money.h"

Money::Money() {
	dollars = 0;
	cents = 0;
}

Money::Money(std::string tmp) {
	parseString(tmp);
}

Money::Money(int dl, int ct) {
	if(ct > 100)
		notSane = true;
	cents = ct;
	dollars = dl;
}

void Money::parseString(std::string tmp) {
	std::stringstream ss;
	auto finder = tmp.find(".");
	if(finder != std::string::npos){
		ss << tmp.substr(0,finder);
		ss >> dollars;
	}
	else
		dollars = 0;

	ss.str("");
	ss.clear();
	
	ss << tmp.substr(finder+1);
	int temp;
	ss >> temp;
	if(temp > 100)
		notSane = true;
	
	cents = temp;
}

Money& Money::operator=(Money& newMoney) {
	
	swap(*this,newMoney);
	return *this;
}

Money& Money::operator=(Money&& o) {
	dollars = std::move(o.dollars);
	cents = std::move(o.cents);
	notSane = std::move(o.notSane);

	return *this;
}

Money& Money::operator+=(Money& newMoney) {
	int temporaryCents;
	int temporaryDollars;

	temporaryDollars = dollars + newMoney.getDollars();
	temporaryCents = cents + newMoney.getCents();
	while(temporaryCents >= 100) {
		temporaryDollars++;
		temporaryCents -= 100;
	}
	this->setDollars(temporaryDollars);
	this->setCents(temporaryCents);

	return *this;
}

Money& Money::operator*=( int mult) {
	int nDollars = dollars*mult;
	int nCents = cents*mult;
	
	while(nCents >= 100){
		nDollars++;
		nCents -= 100;
	}
	
	this->setDollars(nDollars);
	this->setCents(nCents);

	return *this;
}

Money& Money::operator*=( double mult) {
	double nDollars = dollars*mult;
	int nCents = (int)(cents*mult);

	if(nDollars < 1 && nDollars > 0){
		nCents += (int)(nDollars*100);
		nDollars = 0;
	}
	
	while(nCents >= 100){
		nDollars++;
		nCents -= 100;
	}
	
	this->setDollars(nDollars);
	this->setCents(nCents);

	return *this;
}

Money& Money::operator-=(Money& newMoney) {
	int nDollars = dollars - newMoney.getDollars();
	int nCents = cents - newMoney.getCents();
	
	while(nCents >= 100){
		nDollars++;
		nCents -= 100;
	}
	
	this->setDollars(nDollars);
	this->setCents(nCents);

	return *this;
}

bool Money::checkSanity() {
	return notSane;
}

int Money::getDollars() const {
	return dollars;
}

int Money::getCents() const {
	return cents;
}

std::string Money::toString() {
	std::stringstream ss;
	ss << dollars << "." << cents;
	return ss.str();
}

void Money::setDollars(int nDollars) {
	dollars = nDollars;
}

void Money::setCents(int nCents) {
	cents = nCents;
}